import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    const title = exception.name;
    const message = exception.message;
    const detail = exception.stack;
    response.status(status).render('error', {
      title: title,
      message: message,
      status: status,
      path: request.url,
      detail: detail,
      time: new Date().toISOString(),
    });
  }
}
