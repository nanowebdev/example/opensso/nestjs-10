import { Controller, Get, Req, Res } from '@nestjs/common';

@Controller()
export class HomeController {
  @Get()
  root(@Req() req, @Res() res) {
    if (req.query && req.query.token) {
      // save to session
      req.session.token = req.query.token;
      return res.status(302).redirect('/dashboard');
    } else {
      return res.render('home', {
        title: 'Homepage',
        message: 'This is homepage',
      });
    }
  }
}
