import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { HomeController } from './controllers/home.controller';
import { DashboardController } from './controllers/dashboard.controller';
import { LogoutController } from './controllers/logout.controller';
import { JwtMiddleware } from './middlewares/jwt.middleware';
import { SessionMiddleware } from './middlewares/session.middleware';
import { JwtModule } from '@nestjs/jwt';
import { join } from 'path';
import * as fs from 'fs';

@Module({
  imports: [
    JwtModule.register({
      publicKey: fs.readFileSync(join(__dirname, '..') + '/public.key', 'utf8'),
      privateKey: fs.readFileSync(
        join(__dirname, '..') + '/private.key',
        'utf8',
      ),
    }),
  ],
  controllers: [HomeController, DashboardController, LogoutController],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(SessionMiddleware, JwtMiddleware).forRoutes(
      DashboardController,
      // add another protected page that required login
    );
  }
}
