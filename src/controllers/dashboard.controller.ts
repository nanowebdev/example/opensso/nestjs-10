import { Controller, Get, Req, Res } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Controller('dashboard')
export class DashboardController {
  constructor(private jwtService: JwtService) {}
  @Get()
  root(@Req() req, @Res() res) {
    const payload = this.jwtService.decode(req.session.token);
    return res.render('dashboard', {
      title: 'Dashboard page',
      message: 'This is dashboard',
      jwt: payload,
    });
  }
}
