import { Controller, Get, Req, Res } from '@nestjs/common';

@Controller('logout')
export class LogoutController {
  @Get()
  index(@Req() req, @Res() res) {
    req.session.token = null;
    return res.status(302).redirect('/');
  }
}
