import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class SessionMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const session = JSON.parse(JSON.stringify(req.session));
    if (session) {
      const token = session.token;
      if (token) {
        return next();
      }
    }
    return res.status(302).redirect('/');
  }
}
