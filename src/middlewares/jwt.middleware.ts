import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';
import { join } from 'path';
import * as fs from 'fs';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) {}
  use(req: Request, res: Response, next: NextFunction) {
    const session = JSON.parse(JSON.stringify(req.session));
    const token = session.token;
    try {
      this.jwtService.verify(token, {
        publicKey: fs.readFileSync(
          join(__dirname, '..', '..') + '/public.key',
          'utf8',
        ),
        algorithms: ['RS256'],
        maxAge: '8h',
      });
      return next();
    } catch (e) {
      return res.status(302).redirect('/logout');
    }
  }
}
